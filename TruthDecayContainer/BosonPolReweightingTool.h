#ifndef TruthDecayContainer_BosonPolReweightingTool_h
#define TruthDecayContainer_BosonPolReweightingTool_h

// xAOD includes
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"

// ROOT include(s):
#include "TH2.h"
#include "TF1.h"

// CP/ASG tools
#include "TauAnalysisTools/TauTruthMatchingTool.h"
#include "AsgTools/AsgMetadataTool.h"

// C++ includes
#include <vector>
#include <iostream>
#include <algorithm>

// TruthDecayContainer
#include "TruthDecayContainer/DecayHandle.h"

class StatusCode;

typedef TLorentzVector tlv;

class BosonPolReweightingTool 
{

 private:
 
 public:
  BosonPolReweightingTool();
  ~BosonPolReweightingTool();

  ///
  /// Initialize all tools needed for this class
  ///
  StatusCode init(int dsid=0); // dsid=0 -> will find the dsid from FileMetadata

  void loadHistograms(TFile* fin, std::map<TString, TH2F*> &histDict);
  
  // Utility for retrieving coefficients from histograms
  float fetchCoefficients_fixedTanb(const TString &proc, 
				    const float &mHeavy, const float &mLight, 
				    const int &tanb_i, const int &sgnMu);      
  
  // Polarization weight evaluation
  float getPolWeight(const TString &proc, const float &mHeavy, const float &mLight, const float &cosThetaStar, 
		     bool verbose=false);

  std::vector<std::pair<float,float> > getPolWeights(TruthEvent_XX *evt, bool verbose=false);
  std::vector<std::pair<float,float> > getPolWeights(const xAOD::TruthParticleContainer *truthParticles, 
						     bool verbose=false);
  
  // In case you want to go with one line
  float getCombinedPolWeight(TruthEvent_XX *evt, bool verbose=false);
  float getCombinedPolWeight(const xAOD::TruthParticleContainer *truthParticles, bool verbose=false);
  
  
 protected:
  float                    m_convertFromMeV;
  TF1*                     m_func_cosThetaStar;
  std::map<TString,TH2F*>  m_spinMap;
  DecayHandle*             m_decayHandle;

  
};

#endif
