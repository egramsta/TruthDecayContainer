#ifndef TRUTHDECAYCONTAINER_H
#define TRUTHDECAYCONTAINER_H

#include <TruthDecayContainer/Decay_boson.h>
#include <TruthDecayContainer/Decay_tau.h>
#include <TruthDecayContainer/Decay_H.h>

#include <TruthDecayContainer/TruthEvent_Vjets.h>
#include <TruthDecayContainer/TruthEvent_VV.h>
#include <TruthDecayContainer/TruthEvent_VVV.h>
#include <TruthDecayContainer/TruthEvent_TT.h>
#include <TruthDecayContainer/TruthEvent_XX.h>
#include <TruthDecayContainer/TruthEvent_GG.h>


#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ class Decay_tau+;
#pragma link C++ class Decay_boson+;
#pragma link C++ class Decay_H+;

#pragma link C++ class TruthEvent_Vjets+;
#pragma link C++ class TruthEvent_VV+;
#pragma link C++ class TruthEvent_VVV+;
#pragma link C++ class TruthEvent_TT+;
#pragma link C++ class TruthEvent_XX+;
#pragma link C++ class TruthEvent_GG+;




#endif

#endif
